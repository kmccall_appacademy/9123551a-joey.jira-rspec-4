class Friend
  # TODO: your code goes here!
  def greeting(default = "")
    if default.empty?
      "Hello!"
    else
      "Hello, #{default}!"
    end
  end
end
