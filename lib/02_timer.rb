class Timer

  def initialize
    @seconds = 0
  end

  def seconds
    @seconds
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def time_string
    @seconds.time_format
  end

end

class Integer

  def time_format
    seconds = self % 60
    minutes = self / 60 % 60
    hours = self / 3600

    hours.time_string + ":" + minutes.time_string + ":" + seconds.time_string
  end

  def time_string
    if self < 10
      "0#{self}"
    else
      "#{self}"
    end
  end

end
