class Dictionary

  def initialize
    @entries = {}
  end

  def entries
    @entries
  end

  def add(element)
    if element.is_a? Hash
      element.each do |key, value|
        @entries[key] = value
      end
    else
      @entries[element] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(word)
    self.keywords.include?(word)
  end

  def find(string)
    @entries.select { |key, value| key.include?(string) }
  end

  def printable
    @entries.sort.map { |key, value| "[#{key}] \"#{value}\"" }.join("\n")
  end

end
