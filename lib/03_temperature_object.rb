class Temperature

  def initialize(temp)
    @temp = temp
  end

  def in_fahrenheit
    if @temp.has_key?(:f)
      @temp[:f]
    else
      (@temp[:c] * 9.0 / 5) + 32
    end
  end

  def in_celsius
    if @temp.has_key?(:c)
      @temp[:c]
    else
      (@temp[:f] - 32) * 5.0 / 9
    end
  end

  def self.from_celsius(n)
    self.new(c: n)
  end

  def self.from_fahrenheit(n)
    self.new(f: n)
  end

end

class Celsius < Temperature

  def initialize(n, temp = {})
    @temp = temp
    @temp[:c] = n
  end

  def self.from_celsius(n)
    super
  end

  def self.from_fahrenheit(n)
    super
  end

end

class Fahrenheit < Temperature

  def initialize(n, temp = {})
    @temp = temp
    @temp[:f] = n
  end

  def self.from_celsius(n)
    super
  end

  def self.from_fahrenheit(n)
    super
  end

end
