class Book
  # TODO: your code goes here!

  def title
    @title
  end

  def title=(name)
    @title = name.titleize
  end

end

class String

  def titleize
    result = []
    small_words = %w(a an the and but or for of over in)
    string_arr = self.split
    string_arr.each_with_index do |el, idx|
      if small_words.include?(el) && idx != 0
        result << el.downcase
      else
        result << el.capitalize
      end
    end

    result.join(' ')
  end

end
